open Jsutil

type react_instance
type react_factory
type react_element

class type react_component = object
  (*
    lots of magic here, but nothing the wrapper cares about?

    mostly here for type checking :)
  *)
end
(*
type react_state = <
  ocaml: tbd Js.readonly_prop
> Js.t
 *)

type props
(* type wrap_state = State of react_state *)
(*
type react_spec = <
  getInitialState : ('a, wrap_state) Js.meth_callback Js.readonly_prop
> Js.t
 *)
class type react_spec = object
  method render: react_component Js.meth
  method getInitialState: react_component Js.meth
end

class type react = object
  method createClass: react_spec -> react_factory Js.meth
  method renderComponent: react_spec -> Dom_html.element Js.t -> unit Js.meth
  method createElement: react_factory -> 'a -> react_element Js.meth
end

class type reactDOM = object
  method render: react_element -> Dom_html.element Js.t -> unit Js.meth
end

(* Would be nice to not do this in two files *)
let react: react Js.t = Js.Unsafe.variable "React"
let reactDOM: reactDOM Js.t = Js.Unsafe.variable "ReactDOM"

module type Element = sig
  type props
end

module type Factory = sig
  type props
  type state
  val create: props -> react_element
end

(* type updater: details -> unit *)

module type Component = sig
  type props
  type state
  val getInitialState: props -> state
  val render: props -> state -> react_component
end

module CreateClass(Render : Component):
  (Factory with type props := Render.props) =
struct
  type state = Render.state
  type props = Render.props
  let runtimeRender this =
    Render.render this##.props##.ocaml this##.state##.ocaml(* totally unsafe, but hopefully contained *)

  let makeInitialState (state: Render.state) =
    fun _ -> object%js val ocaml = state end

  let create (props: Render.props) =
    let state = Render.getInitialState props in

    let o: react_spec = Js.Unsafe.obj [|
      ("render", Js.Unsafe.inject (Js.wrap_meth_callback runtimeRender));
      ("getInitialState", Js.Unsafe.inject (Js.wrap_meth_callback (makeInitialState state)))
    |]
    (* let o = object%js
      val getInitialState = Js.wrap_meth_callback (makeInitialState state)
      val render = Js.wrap_meth_callback runtimeRender
    end *)
    in

    let factory = react##createClass o in
    (*
      Creates a react element with the new class, and props JS object { ocaml: props }
      It is possible to pass out the ocaml type directly, but it could be a JS array. This seems safer.
    *)
    let props = object%js val ocaml = props end in
    react##createElement factory props
end

let render (c: react_element) container =
  reactDOM##render c container
