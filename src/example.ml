open Jsutil

let () =
  console_log "ocaml is alive!";

  let module MyComponentClass = React.CreateClass (MyComponent) in
  let blue_props = MyComponent.({ name = "World"; location = "Here" }) in
  let green_props = MyComponent.({ name = "World"; location = "There" }) in
  React.render (MyComponentClass.create blue_props) (get_element_by_id "container");
  React.render (MyComponentClass.create green_props) (get_element_by_id "container2")

