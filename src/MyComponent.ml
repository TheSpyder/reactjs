type props = {
  location: string;
  name: string
}

type state = string

let getInitialState props = if props.location = "Here" then "Blue" else "Green"

let render props state = ReactDOM.p ("Hello, " ^ state ^ " " ^ props.name)

