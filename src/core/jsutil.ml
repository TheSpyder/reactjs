(* Aliases and helper functions.
 used by http://try.ocamlpro.com/js_of_ocaml/ lesson 1, step 2
 extracted from:
  https://github.com/OCamlPro/ocp-jslib/blob/master/utils.ml
  https://github.com/OCamlPro/ocp-jslib/blob/master/button.ml

  There's no license on it, so we probably need to ask about that.
*)

let doc = Dom_html.document
let win = Dom_html.window
let window = Dom_html.window

let _s = Js.string
let _f = Js.wrap_callback

let console_log s = Firebug.console##log (_s s)

let force_option o =
  Js.Opt.get o (fun () -> assert false)

let get_element_by_id id =
  (*
    the assert false means that asking for an element that doesn't exist fails with a stack trace
  *)
  force_option (doc##getElementById (_s id))

(* end copied *)

let s_ s = Js.to_string s

let string_to_string f = (fun jss ->
  s_ jss |> f |> _s
)
