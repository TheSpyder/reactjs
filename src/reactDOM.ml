open Jsutil

(* type content = string | React.react_component list *)

(*
  For now, we just need to flesh this out a bit... see:
  http://ocsigen.org/js_of_ocaml/2.4/manual/bindings
*)
class type react_dom = object
  method p: 'a Js.t -> Js.js_string Js.t -> React.react_component Js.meth
end

(* Would be nice to not do this twice *)
let react_dom: react_dom Js.t = Js.Unsafe.global##.React##._DOM

let p ?(props=[||]) content =
  (* let children = match content with
    | s:string -> _s s
    | i:int -> i
  in *)

  react_dom##p (Js.Unsafe.obj props) (_s content)
