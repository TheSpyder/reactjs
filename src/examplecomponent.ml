(* Same example as https://gist.github.com/jordwalke/67819c91df1552009b22
   but using OCaml's simple module feature (every file is a module) *)

open ReactDOM

(* Component Properties *)
type props = {count: int}

(* Hey, state can be any type! *)
type state = string

(* Initializer *)
let getInitialState props = "neverBeenClicked"

(* Signal handlers - these simply return the next state. *)
let componentWillReceiveProps props (prevProps, prevState) = prevState
let handleClick {props; state; updater} evt = "hasBeenClicked"

(* Render: props and state as arguments, just like we've always wanted *)
let render {props; state; updater} = [
  Div.make
    ~styleString: "Omit to rely on defaults #thanksOCaml - no really, thanks OCaml"
    ~className: ("P:" ^ props.prefix ^ " S:" ^ state)
    ~onClick: (updater handleClick)
    ~children: []
]