js_of_ocaml API for Facebook react.js
=====================================

Notes on the [Wiki](https://bitbucket.org/TheSpyder/reactjs/wiki/Home).

Setting up
----------

    opam install js_of_ocaml
    bower install

Building
--------

    make build

