BUILD=eval `opam config env` && ocamlbuild -I src -use-ocamlfind -plugin-tag 'package(js_of_ocaml.ocamlbuild)'
OUT=example.js

build:
	@${BUILD} -tags 'noinline, debug' ${OUT}

dist: clean
	@${BUILD} -quiet -tags 'opt(3)' ${OUT}

clean:
	ocamlbuild -quiet -clean

.PHONY: build dist sublime clean
